# MTweet

#Para que funcione la libreria de PHP Composer:
- En la carpeta app del proyecto generar un archivo llamado **composer.json** y ejecutar los siguentes comandos (mediante powershell si usas windows).

        - composer install
        
        - composer update (en principio solo hace falta ejecutar este comando cuando se añade una nueva libreria en el composer.json).

#Para que funcione con docker:
- Instalar dokcer en el pc. (Es probable que si usas windows te de pida instal·lar otra cosa para que funcione docker. Debe instalarse).

-Una vez instalado docker. En la carpeta app/ del proyecto hay que ejecutar 
acceder a la raiz del proyecto y una vez en la raiz ejecutar el comando: 

    > docker-compose up -d (mediante powershell).

Una vez hecho esto deberia aparecer un mensaje como este:

    > Creating network "mytweetbackend_default" with the default driver
    > Creating api_mytweet_db ... done
    > Creating api_mytweet_www ... done
    > Creating api_mytweet_pma ... done

- Para detener el contenedor docker: 

    > docker-compose down

- Para probar que el servidor ha arranacado correctamente y que está funcionando, en el navegador podemos acceder a la: [siguente url](http://localhost:89/mytweet/hello).
Si todo ha ido bien, deberiamos ver el siguente JSON por pantalla:

    {"message" => "Hello World"}


    RECOMENDABLE: Sincronizar el proyecto con git.


>     Para descargar el proyecto: Pulsar el botón "Clonar" y se puede clonar usando Https o SSH (para que funcione por ssh hay que configurarlo. Para clonar mediante https no hace falta configurar nada)






    
