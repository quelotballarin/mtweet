SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `mytweet_retweet` (
  `retweet_user` varchar(70) NOT NULL,
  `retweet_tweet_id` int(11) NOT NULL,
  `retweeted_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `mytweet_tweet` (
  `tweet_id` int(11) NOT NULL,
  `tweet_text` text NOT NULL,
  `tweet_likes` int(11) NOT NULL DEFAULT 0,
  `tweeted_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `post_user_username` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `mytweet_tweet_like` (
  `tweet_id` int(11) NOT NULL,
  `username` varchar(70) NOT NULL,
  `liked_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `mytweet_user` (
  `username` varchar(70) NOT NULL,
  `firstname` varchar(70) NOT NULL,
  `lastname` varchar(70) NOT NULL,
  `second_lastname` varchar(70) NOT NULL,
  `email` varchar(150) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `registered_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `verified` tinyint(4) NOT NULL DEFAULT 0,
  `avatar` varchar(255) DEFAULT '/var/www/app/images/avatar/default.png'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `mytweet_user_token` (
  `id_token` int(11) NOT NULL,
  `username` varchar(70) NOT NULL,
  `token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `mytweet_retweet`
  ADD PRIMARY KEY (`retweet_user`,`retweet_tweet_id`),
  ADD KEY `FK_RETWEET_TWEET` (`retweet_tweet_id`);


ALTER TABLE `mytweet_tweet`
  ADD PRIMARY KEY (`tweet_id`),
  ADD KEY `FK_USER_POST_TWEET` (`post_user_username`);


ALTER TABLE `mytweet_tweet_like`
  ADD PRIMARY KEY (`tweet_id`,`username`),
  ADD KEY `fk_like_username` (`username`);


ALTER TABLE `mytweet_user`
  ADD PRIMARY KEY (`username`);


ALTER TABLE `mytweet_user_token`
  ADD PRIMARY KEY (`id_token`),
  ADD KEY `fk_username_token` (`username`);


ALTER TABLE `mytweet_tweet`
  MODIFY `tweet_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `mytweet_user_token`
  MODIFY `id_token` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `mytweet_retweet`
  ADD CONSTRAINT `FK_RETWEET_TWEET` FOREIGN KEY (`retweet_tweet_id`) REFERENCES `mytweet_tweet` (`tweet_id`),
  ADD CONSTRAINT `FK_RETWEET_USER` FOREIGN KEY (`retweet_user`) REFERENCES `mytweet_user` (`username`);


ALTER TABLE `mytweet_tweet`
  ADD CONSTRAINT `FK_USER_POST_TWEET` FOREIGN KEY (`post_user_username`) REFERENCES `mytweet_user` (`username`);


ALTER TABLE `mytweet_tweet_like`
  ADD CONSTRAINT `fk_like_tweet_id` FOREIGN KEY (`tweet_id`) REFERENCES `mytweet_tweet` (`tweet_id`),
  ADD CONSTRAINT `fk_like_username` FOREIGN KEY (`username`) REFERENCES `mytweet_user` (`username`);


ALTER TABLE `mytweet_user_token`
  ADD CONSTRAINT `fk_username_token` FOREIGN KEY (`username`) REFERENCES `mytweet_user` (`username`);

COMMIT;

