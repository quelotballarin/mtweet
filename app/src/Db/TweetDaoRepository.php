<?php 

namespace MyTweet\Db;


class TweetDaoRepository extends \MyTweet\Db\DbConnection {

    protected static $instance = null;

    private $tables = [
        "tweets" => DB_TWEET_TABLE
    ];

    protected function __construct() {
        parent::__construct();
    }

    public static function getInstance(): \MyTweet\Db\TweetDaoRepository {
        if(is_null(static::$instance)) {
            static::$instance = new \MyTweet\Db\TweetDaoRepository();
        }

        return static::$instance;
    }

    public function getTweets() {
        $sql = "SELECT * FROM " . $this->tables['tweets'];

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute();

            if(!$result) return null;

            $tweets = [];

            if($stmt->rowCount() > 0) {
                foreach($stmt->fetchAll(\PDO::FETCH_ASSOC) as $tweetAssoc) {
                    $tweets[] = \MyTweet\Model\Tweet::fromAssoc($tweetAssoc);
                }
            }
            
            return $tweets;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    public function getTweetById(int $id) {
        $sql = "SELECT * FROM " . $this->tables['tweets'] . " WHERE tweet_id = :tweet_id";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":tweet_id" => $id
            ]);

            if(!$result) return null;

            if($stmt->rowCount() > 0) {
                return \MyTweet\Model\Tweet::fromAssoc($stmt->fetch(\PDO::FETCH_ASSOC));
            }

            return null;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    private function insertTweet(\MyTweet\Model\Tweet $tweet): ?\MyTweet\Model\Tweet {

        $sql = "INSERT INTO "  . $this->tables['tweets'] . " 
        (tweet_text, tweet_likes, post_user_username) 
        VALUES (:tweet_text, :tweet_likes, :post_user_username)";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":tweet_text" => $tweet->getText(),
                ":tweet_likes" => $tweet->getLikes(),
                ":post_user_username" => $tweet->getUsername()
            ]);

            if(!$result) return null;

            $id = $this->con->lastInsertId();

            $tweet->setId($id);

            return $this->getTweetById($id);
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    private function updateTweet(\MyTweet\Model\Tweet $tweet): ?\MyTweet\Model\Tweet {
        $sql = "UPDATE " . $this->tables['tweets'] . " SET 
        tweet_text = :tweet_text, tweet_likes = :tweet_likes, 
        tweeted_at = :tweeted_at, post_user_username = :post_user_username
        WHERE tweet_id = :tweet_id";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":tweet_text" => $tweet->getText(),
                ":tweet_likes" => $tweet->getLikes(),
                "tweeted_at" => $tweet->getTweetedAt(),
                ":post_user_username" => $tweet->getUsername(),
                ":tweet_id" => $tweet->getId()
            ]);

            if(!$result) return null;

            return $tweet;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    public function persistTweet(\MyTweet\Model\Tweet $tweet): \MyTweet\Model\Tweet {
        if(is_null($tweet->getId())) {
            return $this->insertTweet($tweet);
        }

        return $this->updateTweet($tweet);
    }

    public function deleteTweet(\MyTweet\Model\Tweet $tweet): bool {
        $sql = "DELETE FROM " . $this->tables['tweets'] . "WHERE tweet_id = :tweet_id";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":tweet_id" => $tweet->getId()
            ]);

            return $result;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    
}


?>