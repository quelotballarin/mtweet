<?php 

namespace MyTweet\Db;

class TokenDaoRepository extends \MyTweet\Db\DbConnection {

    protected static $instance = null;

    private $table;

    public static function getInstance(): \MyTweet\Db\TokenDaoRepository{
        if(is_null(static::$instance)) {
            static::$instance = new \MyTweet\Db\TokenDaoRepository();
        }

        return static::$instance;
    }

    protected function __construct() {
        parent::__construct();
        $this->table = \DB_USER_TOKEN;
    }

    private function insertToken(\MyTweet\Model\Token $token): ?\MyTweet\Model\Token {
        $sql = "INSERT INTO " . $this->table . " (username, token) VALUES (:username, :token)";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":username" => $token->getUsername(),
                ":token" => $token->getToken()
            ]);

            if(!$result) return null;

            $token->setId($this->con->lastInsertId());

            return $token;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    private function updateToken(\MyTweet\Model\Token $token): ?\MyTweet\Model\Token {
        $sql = "UPDATE " . $this->table . " SET token = :token = WHERE id_token = :id_token";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":token" => $token->getToken(),
                ":id_token" => $token->getId()
            ]);

            if(!$result) return null;

            return $token;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    public function persistToken(\MyTweet\Model\Token $token): ?\MyTweet\Model\Token {
        if(is_null($token->getId())) {
            return $this->insertToken($token);
        }

        return $this->updateToken($token);
    }

    public function getTokens(\MyTweet\Model\User $user): ?array {
        $sql = "SELECT * FROM " . $this->table . " WHERE username = :username";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":username" => $user->getUsername()
            ]);

            if(!$result) return null;

            if($stmt->rowCount() > 0) {
                $tokens = [];

                foreach($stmt->fetchAll(\PDO::FETCH_ASSOC) as $tokenAssoc) {
                    array_push($tokens, \MyTweet\Model\Token::fromAssoc($tokenAssoc));
                }

                return $tokens;
            }

            return [];
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

}