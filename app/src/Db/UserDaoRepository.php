<?php

namespace MyTweet\Db;

class UserDaoRepository extends \MyTweet\Db\DbConnection {

    protected static $instance = null;

    private $table;

    public static function getInstance(): \MyTweet\Db\UserDaoRepository {
        if(is_null(static::$instance)) {
            static::$instance = new \MyTweet\Db\UserDaoRepository();
        }

        return static::$instance;
    }

    protected function __construct() {
        parent::__construct();     
        $this->table = \DB_USER_TABLE;
    }

    public function getUsers(): ?array {
        $sql = "SELECT * FROM " . $this->table;

        try {
            $stmt = $this->con->query($sql);

            if(!$stmt) return null;

            $users = [];

            if($stmt->rowCount() > 0) {
                foreach($stmt->fetchAll(\PDO::FETCH_ASSOC) as $userAssoc) {
                    array_push($users, \MyTweet\Model\User::fromAssoc($userAssoc));
                }
            }

            return $users;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    public function getUserByUsername(string $username): ?\MyTweet\Model\User {
        $sql = "SELECT * FROM " . $this->table . " WHERE username = :username";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":username" => $username
            ]);

            if(!$result) return null;

            if($stmt->rowCount() > 0) {
                $userAssoc = $stmt->fetch(\PDO::FETCH_ASSOC);
                $user = \MyTweet\Model\User::fromAssoc($userAssoc);
                $token = $this->getLastGeneratedToken($user);
                if(!is_null($token)) {
                    $user->setToken($token);
                }
                return $user;
            }

            return null;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    public function getUserByToken(string $token) {
        $sql = "SELECT mytweet_user.username, firstname, lastname, second_lastname, email, 
        registered_at, verified, avatar, token 
        FROM " . $this->table . " INNER JOIN mytweet_user_token mytut
        WHERE mytut.token = :token";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":token" => $token
            ]);

            if(!$result) return null;

            if($stmt->rowCount() > 0) {
                $dataAssoc = $stmt->fetch(\PDO::FETCH_ASSOC);

                $user = \MyTweet\Model\User::fromAssoc($dataAssoc);

                return $user;
            }

            return null;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    public function getLastGeneratedToken(\MyTweet\Model\User $user): ?string {
        $sql = "SELECT * FROM mytweet_user_token 
        WHERE id_token = (SELECT max(id_token) FROM mytweet_user_token 
        WHERE username = :username)";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":username" => $user->getUsername()
            ]);

            if(!$result) return null;

            if($stmt->rowCount() > 0) {
                $data = $stmt->fetch(\PDO::FETCH_ASSOC);

                return isset($data['token']) && !is_null($data['token']) ? $data['token'] : null;
            }

            return null;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    private function insertUser(\MyTweet\Model\User $user): ?\MyTweet\Model\User {
        $sql = "INSERT INTO " . $this->table . " (username, firstname, lastname, second_lastname, email, pass) 
        VALUES (:username, :firstname, :lastname, :second_lastname, :email, :pass)";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":username" => $user->getUsername(),
                ":firstname" => $user->getFirstname(),
                ":lastname" => $user->getLastname(),
                ":second_lastname" => $user->getSecondLastname(),
                ":email" => $user->getEmail(),
                ":pass" => $user->getPassword()
            ]);

            if(!$result) return null;

            return $user;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    private function updateUser (\MyTweet\Model\User $user): ?\MyTweet\Model\User {
        $sql = "UPDATE " . $this->table . " SET firstname = :firstname, lastname = :lastname, 
        second_lastname = :second_lastname, email = :email, pass = :pass WHERE username = :username";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":username" => $user->getUsername(),
                ":firstname" => $user->getFirstname(),
                ":lastname" => $user->getLastname(),
                ":second_lastname" => $user->getSecondLastname(),
                ":email" => $user->getEmail(),
                ":pass" => $user->getPassword(),
            ]);

            if(!$result) return null;

            return $user;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    public function persistUser(\MyTweet\Model\User $user): ?\MyTweet\Model\User {
        try {
            if(is_null($this->getUserByUsername($user->getUsername()))) {
                return $this->insertUser($user);
            }

            return $this->updateUser($user);
        } catch(\MyTweet\Exception\DbConnectionException $e) {
            throw $e;
        }
    }

    public function deleteUser(\MyTweet\Model\User $user): bool {
        $sql = "DELETE FROM " . $this->table . " WHERE username = :username";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([
                ":username" => $user->getUsername()
            ]);

            return $result;
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

    public function getHashedPassword(\MyTweet\Model\User $user): string {
        $sql = "SELECT pass FROM " . $this->table . " WHERE username = :username";

        try {
            $stmt = $this->con->prepare($sql);

            $result = $stmt->execute([":username" => $user->getUsername()]);

            if(!$result) return null;

            $row = $stmt->fetch(\PDO::FETCH_ASSOC);

            return $row['pass'];
        } catch(\PDOException $e) {
            throw new \MyTweet\Exception\DbConnectionException($e->getMessage());
        }
    }

}

?>