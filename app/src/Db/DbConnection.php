<?php 

namespace MyTweet\Db;

use \MyTweet\Logger\MyTweetLogger as Logger;

abstract class DbConnection {

    protected $con;
    protected $error;

    protected function __construct() {
        try {
            $this->con = new \PDO(\DSN_CON, \DATABASE_USER, \DATABASE_PASSWORD);
            $this->con->setAttribute(\PDO::ERRMODE_EXCEPTION, true);
        } catch(\PDOException $e) {
            Logger::getInstance()->error("Database connection failed: " . $e->getMessage());
            $this->error = $e;
            $this->con = null;
        }
    }

    protected function getConnection(): \PDO {
        return $this->con;
    }
    
    protected function setErrorCon($e) {
        $this->error = $e;
    }
}

?>