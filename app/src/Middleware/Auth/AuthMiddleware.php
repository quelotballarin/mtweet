<?php 

namespace MyTweet\Middleware\Auth;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;

class AuthMiddleware implements MiddlewareInterface {

    private static $instance = null;

    private $userRepository;
    
    private $responseFactory;
    
    private $blacklistMethods;

    private $logger;

    private $logService;

    private $token;

    private $unauthorized; //unauthorized response

    private $internalServerError;
    
    private $user;

    private $regExpValidator;
    
    const TOKEN_MAX_LENTH = 200;

    const TOKEN_MIN_LENGTH = 100;

    protected function __construct() {
        
        $this->userRepository = \MyTweet\Db\UserDaoRepository::getInstance();

        $this->responseFactory = new \Slim\Psr7\Factory\ResponseFactory();
        
        $this->unauthorized = $this->responseFactory->createResponse(\MyTweet\Server\Config\ServerStatus::UNAUTHORIZED);

        $this->internalServerError =  $this->responseFactory->createResponse(\MyTweet\Server\Config\ServerStatus::INTERNAL_SERVER_ERROR);

        $this->blacklistMethods = [
            "PUT" => "PUT",
            "PATCH" => "PATCH",
            "DELETE", "DELETE"
        ];

        $this->logger = \MyTweet\Logger\MyTweetLogger::getInstance();

    }
    public static function getInstance() {
        if(is_null(static::$instance)) {
            static::$instance = new \MyTweet\Middleware\Auth\AuthMiddleware();
        }   

        return static::$instance;
    }

    public function process(ServerRequestInterface $request, 
    RequestHandlerInterface $handler): ResponseInterface {
      return $this->validateAuth($request, $handler);
    }

    private function validateAuth(ServerRequestInterface $request, 
        RequestHandlerInterface $handler): ResponseInterface {

        $token = $request->getHeader("Authorization");

        if (empty($token)) {
            sleep(1);
            return $this->unauthorized; 
        }

        // Decode token string
        $token = $token[0];

        if (substr($token, 0, 7) != 'Bearer ') { 
            sleep(1);
            return $this->unauthorized;
        }

        //getting token string
        $token = substr($token, 7); 

        $this->token = $token;

        if(!$this->isValidToken()) {
            sleep(1);
            return $this->unauthorized;
        }

        try {
            $this->user = $this->userRepository->getUserByToken($this->token);
        } catch(ApiException $e) {
            $this->logger->error($e->getMessage());
            $this->user = null;
            //TODO: Register log (Database). 
            sleep(1);
            return $this->internalServerError;
        }

        if(is_null($this->user)) {
            sleep(1);
            return $this->unauthorized;
        }
    
        if(!$this->isUserAllowedRequest($request)) {
            sleep(1);
            return $this->unauthorized;
        }

        $request->withAttribute('user', $this->user);
        
        $response = $handler->handle($request);

        return $response;
        
    }

    private function isUserAllowedRequest(ServerRequestInterface $request): bool {

        $method = $request->getMethod(); //requested method

        $uri = $request->getUri(); //requested url

        $notAllowedMethod = ($method == "PUT" || 
        $method == "PATCH" || $method == "DELETE");

        if($uri == "http://localhost:89/mytweet/login" || 
            $uri == "http://localhost:89/mytweet/sign-in" 
            && $method == "POST") {
            return true;
        }

        if(!$this->user->isVerified()) {
            return false;
        }

        if($notAllowedMethod) {
            return false;
        }
   
        return true;
    }

    private function isValidToken(): bool {
        if(!isset($this->token) || is_null($this->token)) return false;

        if(empty($this->token)) return false;

        if(!is_string($this->token)) return false;

        if(strlen($this->token) < self::TOKEN_MIN_LENGTH || strlen($this->token) > self::TOKEN_MAX_LENTH) return false;

        $pattern = "@[a-zA-Z0-9]@";

        $this->regExpValidator = new \MyTweet\Validators\RegExpValidator(
            ['pattern' => $pattern, 'subject' => $this->token]
        );
        
        if(!$this->regExpValidator->isValidRegexp()) return false;
       
        return true;
    }   


}