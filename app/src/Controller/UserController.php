<?php 

namespace MyTweet\Controller;

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \MyTweet\Service\Token\TokenGeneratorService as TokenGeneratorService;
use \MyTweet\Db\UserDaoRepository as UserRepository;

class UserController extends \MyTweet\Controller\BaseController {

    public static function init($app) {
        $app->get(GET_USERS, "\MyTweet\Controller\UserController:getUsers");
        $app->get(GET_USER_BY_USERNAME, "\MyTweet\Controller\UserController:getUserByUsername");
        $app->get(GET_USER_BY_TOKEN, "\MyTweet\Controller\UserController:getUserByToken");
        $app->get(GET_LAST_GENERATED_TOKEN, "\MyTweet\Controller\UserController:getLastGeneratedToken");
        $app->post(\SIGN_IN, "\MyTweet\Controller\UserController:signIn");
        $app->post(LOGIN, "\MyTweet\Controller\UserController:login");
    }

    public function getUsers(Request $request, Response $response, array $args) {
        try {
          $users = UserRepository::getInstance()->getUsers();
        
          if(is_null($users)) {
              return $response->withJson(["error" => "Error getting users list from database"], \MyTweet\Server\Config\ServerStatus::INTERNAL_SERVER_ERROR);
          }

          return $response->withJson($users, \MyTweet\Server\Config\ServerStatus::OK);
        } catch(\Exception $e) {
            $this->catchException($e, $response);     
        }
    }

    public function getUserByUsername(Request $request, Response $response, array $args) {
        $username = $this->getVal($args['username']);

        if(is_null($username)) {
            return $response->withJson(["error" => "Requested username"], \MyTweet\Server\Config\ServerStatus::BAD_REQUEST);
        }

        try {
            $user = UserRepository::getInstance()->getUserByUsername($username);

            if(is_null($user)) {
                return $response->withJson(
                    ["error" => "Error getting user from database. User <<" . \trim($username) . ">> does not exists"], 
                    \MyTweet\Server\Config\ServerStatus::NOT_FOUND);
            }
  
            return $response->withJson($user, \MyTweet\Server\Config\ServerStatus::OK);
        } catch(\Exception $e) {
            $this->catchException($e, $response);
        }
    }

    public function getUserByToken(Request $request, Response $response, array $args) {
        $token = $this->getVal($args['token']);
        
        if(is_null($token)) {
            return $response->withJson(["error" => "Requested token"], \MyTweet\Server\Config\ServerStatus::BAD_REQUEST);
        }

        try {
            $userWithToken = UserRepository::getInstance()->getUserByToken($token);

            if(is_null($userWithToken)) {
                return $response->withJson(['error' => "Request user was not found"], \MyTweet\Server\Config\ServerStatus::NOT_FOUND);
            }

            return $response->withJson($userWithToken, \MyTweet\Server\Config\ServerStatus::OK);
        } catch(\Exception $e) {
            $this->catchException($e, $response);
        }
    }

    public function getLastGeneratedToken(Request $request, Response $response, array $args) {
        $username = $this->getVal($args['username']);

        if(is_null($username)) {
            return $response->withJson(["error" => "Requested username"], \MyTweet\Server\Config\ServerStatus::BAD_REQUEST);
        }

        try {
            $user = UserRepository::getInstance()->getUserByUsername($username);

            if(is_null($user)) {
                return $response->withJson(["error" => "Requested user was not found"], \MyTweet\Server\Config\ServerStatus::NOT_FOUND);
            }

            $lastGeneratedToken = UserRepository::getInstance()->getLastGeneratedToken($user);

            if(is_null($lastGeneratedToken)) {
                return $response->withJson(["error" => "User hasn't got tokens"], \MyTweet\Server\Config\ServerStatus::NOT_FOUND);
            }

            return $response->withJson(["token" => $lastGeneratedToken], \MyTweet\Server\Config\ServerStatus::OK);
        } catch(\Exception $e) {
            $this->catchException($e, $response);
        }
    }

    public function signIn(Request $request, Response $response, array $args) {
        $data = $request->getParsedBody();

        try {
            if(\MyTweet\Validators\SignInValidator::validData($data)) {
                
                $requestData = \MyTweet\Validators\SignInValidator::addDefaultDataValues($data);

                $requestUser = \MyTweet\Model\User::fromAssoc($requestData);

                $requestUser->setPassword($this->getVal($requestData['pass']));

                try {
                    $newUser = UserRepository::getInstance()->persistUser($requestUser);

                    if(is_null($newUser)) {
                        return $response->withJson(["error" => "Failed to save user on database"], \MyTweet\Server\Config\ServerStatus::INTERNAL_SERVER_ERROR);
                    }

                    return $response->withJson($newUser, \MyTweet\Server\Config\ServerStatus::OK);
                } catch(\MyTweet\Exception\DbConnectionException $e) {
                    return $response->withJson(["error" => $e->getMessage()], \MyTweet\Server\Config\ServerStatus::INTERNAL_SERVER_ERROR);
                }
            }
        } catch(\MyTweet\Exception\MyTweetException $e) {
            \MyTweet\Logger\MyTweetLogger::getInstance()->error($e->getMessage());
            return $response->withJson(["error" => $e->getMessage()], \MyTweet\Server\Config\ServerStatus::BAD_REQUEST);
        }
    }

    public function login(Request $request, Response $response, array $args) {
        $data = $request->getParsedBody();

        try {
            if(\MyTweet\Validators\LoginValidator::isValid($data)) {
                try {
                    $requestLoginUser = UserRepository::getInstance()->getUserByUsername($data['username']);

                    if(is_null($requestLoginUser)) {
                        return $response->withJson(["error" => "Requested login user not found"], \MyTweet\Server\Config\ServerStatus::NOT_FOUND);
                    }
    
                    $token = TokenGeneratorService::generateToken();
    
                    $tokenObj = new \MyTweet\Model\Token(null, $requestLoginUser->getUsername(), $token);

                    $requestToken = \MyTweet\Db\TokenDaoRepository::getInstance()->persistToken($tokenObj);
    
                    if(is_null($requestToken)) {
                        return $response->withJson(["error" => "Operation failed. Cannot save authentication token successfully. Try again."], \MyTweet\Server\Config\ServerStatus::INTERNAL_SERVER_ERROR);
                    }

                    $requestLoginUser->setToken($requestToken->getToken());

                    return $response->withJson($requestLoginUser, \MyTweet\Server\Config\ServerStatus::OK);
                }  catch(\MyTweet\Exception\DbConnectionException $e) {
                    return $response->withJson(['error' => "Failed to connect to database. Try again later"], \MyTweet\Server\Config\ServerStatus::INTERNAL_SERVER_ERROR);
                }
            }
        } catch(\MyTweet\Exception\MyTweetException $e) {
            \MyTweet\Logger\MyTweetLogger::getInstance()->error($e->getMessage());
            return $response->withJson(["error" => $e->getMessage()], \MyTweet\Server\Config\ServerStatus::BAD_REQUEST);
        }
      
    }
}