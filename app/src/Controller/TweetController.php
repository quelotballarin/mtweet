<?php 

namespace MyTweet\Controller;

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \MyTweet\Db\TweetDaoRepository as TweetRepository;
use \MyTweet\Validators\TweetValidator;
use \MyTweet\Db\TokenDaoRepository as TokenDaoRepository;
use \MyTweet\Db\UserDaoRepository as UserDaoRepository;

class TweetController extends \MyTweet\Controller\BaseController {
   
    public static function init($app) {
        $app->get(GET_TWEETS, "\MyTweet\Controller\TweetController:getTweets");
        $app->get(GET_TWEET_BY_ID, "\MyTweet\Controller\TweetController:getTweetById");
        $app->post(POST_TWEET, "\MyTweet\Controller\TweetController:postTweet");
    }
  
   public function getTweets(Request $request, Response $response, array $args) {
        try {
            $tweets = TweetRepository::getInstance()->getTweets();

            if(is_null($tweets)) {
                return $response->withJson(["error" => "Cannot get tweets from database"], \MyTweet\Server\Config\ServerStatus::INTERNAL_SERVER_ERROR);
            }

            return $response->withJson($tweets, \MyTweet\Server\Config\ServerStatus::OK);
        } catch(\MyTweet\Exception\DbConnectionException $e) {
            $this->catchException($e, $response);
        }
   }

   public function getTweetById(Request $request, Response $response, array $args) {
        $id = $this->getVal($args['id']);

        if(is_null($id)) {
            return $response->withJson(['error' => "Required tweet id"], \MyTweet\Server\Config\ServerStatus::BAD_REQUEST);
        }

        try {
            $tweet = TweetRepository::getInstance()->getTweetById(intval($id));

            if(is_null($tweet)) {
                return $response->withJson(['error' => "Tweet was not found"], \MyTweet\Server\Config\ServerStatus::NOT_FOUND);
            }
    
            return $response->withJson($tweet, \MyTweet\Server\Config\ServerStatus::OK);
        } catch(\MyTweet\Exception\DbConnectionException $e) {
            $this->catchException($e, $response);
        }
   }

   public function postTweet(Request $request, Response $response, array $args) {
       $data = $request->getParsedBody();
       
       if(!isset($data['tweet_id'])) {
            $data['tweet_id'] = null;
        }
       
        if(!isset($data['tweeted_at'])) {
            $data['tweeted_at'] = null;
        }

       $tweetValidator = new \MyTweet\Validators\TweetValidator($data);

       if(!$tweetValidator->isValidTweet()) {
           return $response->withJson(["error" => "Invalid tweet data"], \MyTweet\Server\Config\ServerStatus::BAD_REQUEST);
       }

       try {
            $requestTweet = \MyTweet\Model\Tweet::fromAssoc($data);

            $persistedTweet = TweetRepository::getInstance()->persistTweet($requestTweet);

            if(is_null($persistedTweet)) {
                return $response->withJson(["error" => "Tweet cannot be saved successfully. Try again."], \MyTweet\Server\Config\ServerStatus::INTERNAL_SERVER_ERROR);
            }

            return $response->withJson($persistedTweet, \MyTweet\Server\Config\ServerStatus::OK);
       } catch(\Exception $e) {
            $this->catchException($e, $response);
       }
   }

   public function updateTweet(Request $request, Response $response, array $args) {
        $id = $this->getVal($args['id']);

        $data = $request->getParsedBody();

        try {
            $requestedTweet = TweetRepository::getInstance()->getTweetById($id);

            if(is_null($requestedTweet)) {
                return $response->withJson(["error" => "Requested tweet was not found"], \MyTweet\Server\Config\ServerStatus::NOT_FOUND);
            }

            $tweetUser = UserDaoRepository::getInstance()->getUserByUsername($requestedTweet->getUsername());
            
            if(is_null($tweetUser)) {
                return $response->withJson(["error" => "Requested user was not found"], \MyTweet\Server\Config\ServerStatus::NOT_FOUND);
            }

            $tweetValidator = new TweetValidator($data);

            if(!$tweetValidator->hasValidText()) {
                return $response->withJson(["error" => "Invalid tweet data"], \MyTweet\Server\Config\ServerStatus::BAD_REQUEST);
            }

            $requestedTweet->setText($data['tweet_text']);

            $updatedTweet = TweetRepository::getInstance()->persistTweet($requestedTweet);

            if(is_null($updatedTweet)) {
                return $response->withJson(["error" => "Tweet cannot be updated successfully"], \MyTweet\Server\Config\ServerStatus::INTERNAL_SERVER_ERROR);
            }

            return $response->withJson($updatedTweet,  \MyTweet\Server\Config\ServerStatus::OK);
        } catch(\Exception $e) {
            $this->catchException($e, $response);
        }
   }

   public function deleteTweet(Request $request, Response $response, array $args) {
        $id = $this->getVal($args['id']);

        if(is_null($id)) {
            return $response->withJson(["error" => "Request tweet identifier"], \MyTweet\Server\Config\ServerStatus::BAD_REQUEST);
        }

        try {
            $requestedTweet = TweetRepository::getInstance()->getTweetById($id);

            if(is_null($requestedTweet)) {
                return $response->withJson(["error" => "Reqested tweet was not found"], \MyTweet\Server\Config\ServerStatus::NOT_FOUND);
            }

            $result = TweetRepository::getInstance()->deleteTweet($requestedTweet);

            return $result 
            ? $response->withJson(["message" => "Tweet has been deleted"], \MyTweet\Server\Config\ServerStatus::OK) 
            : $response->withJson(["error" => "Tweet cannot be deleted"], \MyTweet\Server\Config\ServerStatus::INTERNAL_SERVER_ERROR); 
        } catch(\Exception $e) {
            $this->catchException($e, $response);
        } 
   }

   public function retweet(Request $request, Response $response, array $args) {
        
   }

}

?>