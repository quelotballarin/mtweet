<?php 

namespace MyTweet\Controller;

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class HelloWorldController {

    public static function init($app) {
        $app->get(HELLO_WORLD_TEST, "\MyTweet\Controller\HelloWorldController:test");
        $app->get(HELLO_WORLD_PARAM_TEST, "\MyTweet\Controller\HelloWorldController:testWithUrlParam");
    }

    public function test(Request $request, Response $response, array $args) {
        return $response->withJson(["message" => "Hello world"], 200);
    }

    public function testWithUrlParam(Request $request, Response $response, array $args) {
        if(isset($args['name'])) {
            $name = \ucwords(\trim($args['name']));
            return $response->withJson(["message" => "Hello " . $name], 200);
        }

        return $response->withJson(["message" => "Param <<name>> does not exists"], 400);
    }
}