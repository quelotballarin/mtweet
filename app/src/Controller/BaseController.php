<?php 

namespace MyTweet\Controller;

use \Psr\Http\Message\ResponseInterface as Response;

abstract class BaseController {

    public function getVal($arg) {
        return (isset($arg) && !is_null($arg)) ? $arg : null;
    }

    public function catchException(\Exception $e, Response $response) {
        \MyTweet\Logger\MyTweetLogger::getInstance()->error($e->getMessage());
        return $response->withJson(["error" => $e->getMessage()], \MyTweet\Server\Config\ServerStatus::BAD_REQUEST);
    }
}
?>