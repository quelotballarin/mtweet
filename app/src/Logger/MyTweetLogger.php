<?php 

namespace MyTweet\Logger;

use \Monolog\Handler\StreamHandler;
use \Monolog\Logger;

class MyTweetLogger {

    protected static $instance = null;

    private $logMessage;
    
    private $logger;

    public static function getInstance() {
        if(is_null(static::$instance)) {
            try {
                static::$instance = new \MyTweet\Logger\MyTweetLogger();
            } catch(\MyTweet\Exception\FileNotFoundException $e) { }
        }

        return static::$instance;
    }

    protected function __construct() {
        if(!\file_exists(LOGS_FILE)) {
            throw new \MyTweet\Exception\FileNotFoundException("El archivo de logs especificado no existe");
        }

        $this->logger = new Logger('MyTweetApi');
        
        $this->logger->pushHandler(
            new StreamHandler(LOGS_FILE, Logger::DEBUG)
        );
    }

    public function info($logMessage) {
        $this->logMessage = $logMessage;
        $this->logger->info($this->logMessage);
    }

    public function warning($logMessage) {
        $this->logMessage = $logMessage;
        $this->logger->warning($this->logMessage);
    }

    public function error($logMessage) {
        $this->logMessage = $logMessage;
        $this->logger->error($this->logMessage);
    }

    public function getLogMessage() { return $this->logMessage; }
}