<?php 

namespace MyTweet\Service\Date;

class DateService {

    public static function getCurrentDateWithDefaultFormat(): string {
        return date(\CURRENT_DATE_FORMAT, \CURRENT_TIME);
    }

    public static function getCurrentDateWithFormat(string $format) {
        return date($format, \CURRENT_TIME);
    }

    public static function getDateWithFormat(int $time, string $format) {
        return date($format, $time);
    }

}