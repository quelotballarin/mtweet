<?php 

namespace MyTweet\Service\Token;

use \MyTweet\Exception\MyTweetException as MTException;
use \MyTweet\Exception\FileNotFoundException as FileNotFoundException;
use \MyTweet\Logger\MyTweetLogger as Logger;
use \MyTweet\Service\Date\DateService as DateService;

class TokenGeneratorService {

    private const BYTES_LENGTH = 100;

    private static $token;

    public static function generateToken(): string {
        $bytes = openssl_random_pseudo_bytes(self::BYTES_LENGTH);

        $token = bin2hex($bytes);

        static::$token = $token;

        if(\method_exists(TokenGeneratorService::class, 'writeSecurityTokenInFile')) {
            static::writeSecurityTokenInFile();
        }
        
        return $token;
    }

    private static function writeSecurityTokenInFile() {
       try {
            if(\MyTweet\Service\File\FileService::exists(\SECURITY_TOKEN_FILE) &&
            \MyTweet\Service\File\FileService::isFile(\SECURITY_TOKEN_FILE)) {
                
                $tokenData = [
                    "generated_at" => DateService::getCurrentDateWithDefaultFormat(),
                    "token" => static::$token
                ];

                $jsonStringTokenData = json_encode($tokenData);
                
                \MyTweet\Service\File\FileService::write(\SECURITY_TOKEN_FILE, $jsonStringTokenData . "\n");
            }
       } catch(\Exception $e) {
            \MyTweet\Logger\MyTweetLogger::getInstance()->error($e->getMessage());
       }
    }
}

?>