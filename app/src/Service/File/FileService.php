<?php 

namespace MyTweet\Service\File;

class FileService {

    public static function exists(string $filename): bool {
        if(!\file_exists($filename)) {
            throw new \MyTweet\Exception\FileNotFoundException(basename($filename) . "was not found. File does not exists");
        }

        return true;
    }

    public static function isFile(string $filename): bool {
        if(!\is_file($filename)) {
            throw \MyTweet\Exception\NotAFileException($filename. " is not a file");
        }

        return true;
    }

    public static function write(string $filename, string $content): bool {
        if(\file_put_contents($filename, $content, \FILE_APPEND | \LOCK_EX) !== 1) {
            throw new \MyTweet\Exception\WriteFileException("Cannot write in file " . basename($filename));
        }

        return true;
    }

    public static function read(string $filename): string {
        if(static::exists($filename) && static::isFile($filename)) {
            return \file_get_contents($filename);
        } else {
            throw new \MyTweet\Exception\ReadFileException("Cannot read file " . basename($filename));
        }
    }
}

?>