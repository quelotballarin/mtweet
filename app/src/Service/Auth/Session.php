<?php

namespace MyTweet\Service\Auth;

class Session {

    public function __construct() {
        $this->start();
    }

    private function start() {
        if(!isset($_SESSION)) {
            \session_start();
        }
    }

    public function destroy() {
        if(isset($_SESSION)) {
            \session_destroy();
        }
    }

    public function setData(array $sessionData) {
        if(isset($sessionData) && !empty($sessionData)) {
            foreach($sessionData as $sessionKey => $sessionVal) {
                $_SESSION[$sessionKey] = $sessionVal;
            }
        }
    }

    public function getData() {
        return $_SESSION;
    }

    public function get($key) {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    public function set($key, $value) {
        $_SESSION[$key] = $value;
    }

    public function remove($key) {
        if(isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }

}

?>