<?php 

namespace MyTweet;

use Slim\Factory\AppFactory;

use \MyTweet\Controller\HelloWorldController; 
use MyTweet\Controller\UserController; 
use MyTweet\Controller\TweetController;

class MyTweetApi {

    public static function processRequest() {
        //creating an Slim application.
        $app = AppFactory::create();

        //Controllers
        HelloWorldController::init($app);
        UserController::init($app);
        TweetController::init($app);

        //Middleware
        $app->add(\MyTweet\Middleware\Auth\AuthMiddleware::getInstance()); //authentication
        
        //run app
        $app->run();
    }
}