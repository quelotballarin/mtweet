<?php 

//required autoloader
require_once dirname(__DIR__) . '/vendor/autoload.php';

//changing headers information to prevent CORS Policy problems
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
header('Allow: GET, POST, OPTIONS, PUT, DELETE');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//folders
define("APP_FOLDER", dirname(__DIR__));
define("LOGS_FOLDER", APP_FOLDER . DIRECTORY_SEPARATOR . "/logs");
define("LOGS_FILE", APP_FOLDER . DIRECTORY_SEPARATOR . "logs/myTweetLogs.log");
define('IMAGES_FOLDER', APP_FOLDER . DIRECTORY_SEPARATOR . "images");
define("AVATAR_FOLDER", IMAGES_FOLDER . DIRECTORY_SEPARATOR . "avatar");
define("SECURITY_TOKEN_FILE", APP_FOLDER . "/private/tokens/secret_tokens.txt");

//Db config
define('DATABASE_HOST', "api_mytweet_db");
define("DATABASE_USER", "mytweetuser");
define("DATABASE_PASSWORD", "mytweetpassword");
define("DATABASE_NAME", "mytweetdb");
define("DATABASE_PORT", "3307");
define("DSN_CON", 'mysql:host='. DATABASE_HOST . ';dbname=' . DATABASE_NAME);

define("DB_PREFIX_TABLE_NAME", "mytweet_");
define("DB_USER_TABLE", DB_PREFIX_TABLE_NAME . "user");
define("DB_TWEET_TABLE", DB_PREFIX_TABLE_NAME . "tweet");
define("DB_RETWEET_TABLE", DB_PREFIX_TABLE_NAME ."retweet");
define("DB_TWEET_LIKE_TABLE", DB_PREFIX_TABLE_NAME ."tweet_like");
define("DB_USER_TOKEN", DB_PREFIX_TABLE_NAME . "user_token");

//routes
define("BASE_ENDPOINT", "/mytweet");
define("HELLO_WORLD_TEST", BASE_ENDPOINT . "/hello");
define("HELLO_WORLD_PARAM_TEST",  HELLO_WORLD_TEST . "/{name}");

//user routes
define("GET_USERS", BASE_ENDPOINT . "/users");
define("GET_USER_BY_USERNAME", BASE_ENDPOINT . "/user/{username}");
define('GET_USER_BY_TOKEN', BASE_ENDPOINT . "/user/token/{token}");
define("GET_LAST_GENERATED_TOKEN", BASE_ENDPOINT . "/user/{username}/token/last");
define("SIGN_IN", BASE_ENDPOINT . "/sign-in");
define("LOGIN", BASE_ENDPOINT . "/login");

//tweet routes 
define('GET_TWEETS', BASE_ENDPOINT . "/tweets");
define("GET_TWEET_BY_ID", GET_TWEETS . "/{id}");
define('POST_TWEET', GET_TWEETS);
define('UPDATE_TWEET', GET_TWEET_BY_ID); 

//global 
define("DEFAULT_USER_AVATAR", AVATAR_FOLDER . DIRECTORY_SEPARATOR. "default.png");
define("CURRENT_DATE_FORMAT", "d-m-y H:i:s");
define("DEFAULT_VERIFIED_ACCOUNT_STATUS", 0);
define("CURRENT_TIME", time());

//init ECerdanya api requests
\MyTweet\MyTweetApi::processRequest();