<?php 

namespace MyTweet\Validators;

use \MyTweet\Db\UserDaoRepository as UserRepository;

class LoginValidator {

    public static function isValid(array $data) {
        if(!isset($data) || empty($data)) {
            throw new \MyTweet\Exception\MyTweetException("Invalid requested data");
        }

        foreach(['username', 'pass'] as $key) {
            if(!isset($data[$key])) {
                throw new \MyTweet\Exception\MyTweetException("Invalid data. Required data is missing");
            }
        }

        if(isset($data['username']) && empty($data['username'])) {
            throw new \MyTweet\Exception\MyTweetException("Incorrect login is required");
        }

        try {
            if(is_null(UserRepository::getInstance()->getUserByUsername($data['username']))) {
                throw new \MyTweet\Exception\MyTweetException("Incorrect login. Username or password didn't match");
            }

            $user = UserRepository::getInstance()->getUserByUsername($data['username']);

            $hashedPass = UserRepository::getInstance()->getHashedPassword($user);

            if(is_null($hashedPass)) {
                throw new \MyTweet\Exception\MyTweetException("Incorrect login. Username or password didn't match");
            }

            if(!\password_verify($data['pass'], $hashedPass)) {
                throw new \MyTweet\Exception\MyTweetException("Incorrect login. Username or password didn't match");
            }

            return true;
        } catch(\MyTweet\Exception\DbConnectionException $e) {
            \MyTweet\Logger\MyTweetLogger::getInstance()->error($e->getMessage());
            return false;
        }
    }
}
?>