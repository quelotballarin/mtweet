<?php 

namespace MyTweet\Validators;

class TweetValidator extends \MyTweet\Validators\BaseValidator {

    protected array $data;

    public function __construct(array $data) {
        $this->data = $data;
    }

    public function isValidTweet(): bool {

        if(!$this->isValid($this->data)) {
            return false;
        }

        foreach(["tweet_text", "tweet_likes", "post_user_username"] as $key) {
            if(!isset($this->data[$key])) {
                return false;
            }
        }

        if(intval($this->data['tweet_likes']) < 0) {
            return false;
        }

        if(empty(\trim($this->data['post_user_username'])) || \is_null($this->data['post_user_username'])) {
            return false;
        }

        return true;
    }

    public function hasValidText(): bool {
        return isset($this->data['tweet_text']) && !empty(\trim($this->data['tweet_text']) && \strlen($this->data['tweet_text']) <= 500);
    }

    public function sanitize(): array { return []; }

    public function getSanitizedData(): array { return []; }
}