<?php 

namespace MyTweet\Validators;

abstract class BaseValidator {

    protected array $data;

    function __construct(array $data) {
        $this->resetValidator();

        if(!$this->isValid($data)) {
            throw new ApiException("Invalid data");
        }

        $this->data = $data;
    }

    protected function isValid(array $data): bool {
        return (isset($data) && is_array($data) && !is_null($data) && !empty($data));
    }

    private function resetValidator() {
        if(isset($this->data)) {
            unset($this->data);
        }
    }

    /**
     * Este método debe ser declarado en todas las clases hijas de esta clase.
     */
    protected abstract function sanitize(): array;
    
    protected abstract function getSanitizedData(): array;

}