<?php 

namespace MyTweet\Validators;

class RegExpValidator extends \MyTweet\Validators\BaseValidator {
    public function __construct(array $data) { 
        parent::__construct($data);
    }

    /**
     * @return bool if is valid regex, false if it's not a valid regex
    */
    public function isValidRegexp(): bool {
        if(!isset($this->data['pattern']) || is_null($this->data['pattern']) || 
        empty(trim($this->data['pattern']))) return false;
        
        if(!isset($this->data['subject']) || \is_null($this->data['subject'])) return false;

        return preg_match($this->data['pattern'], $this->data['subject']);
    }

    protected function sanitize(): array {
        return $this->data;
    }

    public function getSanitizedData(): array {
        return $this->sanitize();
    }

}