<?php 

namespace MyTweet\Validators;

use \MyTweet\Validators\BaseValidator;

class SignInValidator extends BaseValidator {

    public static function validData(array $data): bool {
        if(empty($data)) {
            throw new \MyTweet\Exception\MyTweetException("Inalid requested data");
        }

        foreach(["username", "firstname", "lastname", "second_lastname", "email", "pass", "pass_confirm"] as $key) {
            if(!isset($data[$key])) {
                throw new \MyTweet\Exception\MyTweetException("Invalid data. Required data is missing");
            }
        }

        if(isset($data['username']) && empty(trim($data['username']))) {
            throw new \MyTweet\Exception\MyTweetException("Invalid username. Cannot be empty");
        }

        try {
            if(!is_null(\MyTweet\Db\UserDaoRepository::getInstance()->getUserByUsername($data['username']))) {
                throw new \MyTweet\Exception\MyTweetException("Inalid username. This username already exists");
            }
        } catch(\MyTweet\Exception\DbConnectionException $e) {
            throw new \MyTweet\Exception\MyTweetException("Failed to verify username: " . $e->getMessage());
        }

        if(isset($data['firstname']) && empty(trim($data['firstname']))) {
            throw new \MyTweet\Exception\MyTweetException("Inalid firstname. Cannot be empty");
        }

        if(isset($data['lastname']) && empty(trim($data['lastname']))) {
            throw new \MyTweet\Exception\MyTweetException("Inalid lastname. Cannot be empty");
        }

        if(isset($data['second_lastname']) && empty(trim($data['second_lastname']))) {
            throw new \MyTweet\Exception\MyTweetException("Inalid second lastname. Cannot be empty");
        }

        if(isset($data['email']) && empty(trim($data['email']))) {
            throw new \MyTweet\Exception\MyTweetException("Invalid email. Cannot be empty");
        }

        if(!filter_var($data['email'], \FILTER_VALIDATE_EMAIL)) {
            throw new \MyTweet\Exception\MyTweetException("Invalid email. Must have a valid email format");
        }
        
        if(isset($data['pass']) && empty(trim($data['pass']))) {
            throw new \MyTweet\Exception\MyTweetException("Invalid password. Cannot be empty");
        }

        if(isset($data['pass_confirm']) && empty(trim($data['pass_confirm']))) {
            throw new \MyTweet\Exception\MyTweetException("Invalid password confirmation. Cannot be empty");
        }

        if($data['pass'] != $data['pass_confirm']) {
            throw new \MyTweet\Exception\MyTweetException("Invalid password. Passwords didn't match");
        }

        return true;
    }

    public static function addDefaultDataValues(array $data) {
        $data['registered_at'] = null;
        $data['verified'] = \DEFAULT_VERIFIED_ACCOUNT_STATUS;
        $data['avatar'] = \DEFAULT_USER_AVATAR;
        $data['token'] = null;

        return $data;
    }
    
	function sanitize() { }
	
	function getSanitizedData() { }
}

?>