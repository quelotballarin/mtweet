<?php 

namespace MyTweet\Exception;

use \Exception;

class DbConnectionException extends Exception {
    
    public function __construct(string $message) {
        parent::__construct($message);
    }

}
?>