<?php 

namespace MyTweet\Exception;

class WriteFileException extends \Exception {
    
    public function __construct(string $message) {
        parent::__construct($message);
    }
}