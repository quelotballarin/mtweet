<?php 

namespace MyTweet\Exception;

class MyTweetException extends \Exception {
    
    public function __construct(string $message) {
        parent::__construct($message);
    }
}