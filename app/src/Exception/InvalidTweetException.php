<?php 

namespace MyTweet\Exception;

use \Exception;

class InvalidTweetException extends Exception {

    public function __construct(string $message) {
        parent::__construct($message);
    }
}