<?php 

namespace MyTweet\Server\Config;

class ServerStatus {
    
    public const INTERNAL_SERVER_ERROR = 500;
    public const NOT_FOUND = 404;
    public const BAD_REQUEST = 400;
    public const REQUEST_TIMED_OUT = 408;
    public const OK = 200;
    public const UNAUTHORIZED = 401;
    public const UNAUTHENTICATED = 403;
}
?>