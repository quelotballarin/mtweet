<?php 

namespace MyTweet\Model;

class User implements \JsonSerializable {

    private string $username;
    private string $firstname;
    private string $lastname;
    private string $secondLastname;
    private string $email;
    private string $pass;
    private ?string $registeredAt;
    private int $verified;
    private string $avatar;
    private ?string $token;

    public function __construct(string $username, string $firstname, string $lastname, string $secondLastname,
    string $email, ?string $registeredAt, int $verified, string $avatar, ?string $token) {
        $this->username = $username;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->secondLastname = $secondLastname;
        $this->email = $email;
        $this->registeredAt = $registeredAt;
        $this->verified = $verified;
        $this->avatar = $avatar;
        $this->token = $token;
    }

    public function getUsername(): string { return $this->username; }
    public function setUsername(string $username): void { $this->username = $username; }

    public function getFirstname(): string { return $this->firstname; }
    public function setFirstname(string $firstname): void { $this->firstname = $firstname; }

    public function getLastname(): string { return $this->lastname; }
    public function setLastname(string $lastname): void { $this->lastname = $lastname; }

    public function getSecondLastname(): string { return $this->secondLastname; }
    public function setSecondLastname(string $secondLastname): void { $this->secondLastname = $secondLastname; }

    public function getEmail(): string { return $this->email; }
    public function setEmail(string $email): void { $this->email = $email; }

    public function getPassword(): string { return $this->pass; }
    public function setPassword(string $pass): void { $this->pass = password_hash($pass, \PASSWORD_DEFAULT); }

    public function getRegisteredAt(): string {
        return \date(CURRENT_DATE_FORMAT, \strtotime($this->registeredAt));
    }

    public function setRegisteredAt(): void {
        $this->registeredAt = \date(CURRENT_DATE_FORMAT, \strtotime($registeredAt));
    }

    public function isVerified(): bool { return ($this->verified === 1); }
    public function setVerified(int $verified): void { $this->verified = $verified; }

    public function getAvatar(): string { return $this->avatar; }
    public function setAvatar($avatar): void { $this->avatar = $avatar; }

    public function getToken(): ?string { return $this->token; }
    public function setToken(?string $token) { $this->token = $token; }

    public function jsonSerialize() {
        return [
            "username" => $this->username,
            "firstname" => $this->firstname,
            "lastname" => $this->lastname,
            "secondLastname" => $this->secondLastname,
            "email" => $this->email,
            "registeredAt" => $this->getRegisteredAt(),
            "verified" => $this->isVerified(),
            "avatar" => $this->avatar,
            "token" => $this->token
        ];
    }

    public static function fromAssoc(array $data): \MyTweet\Model\User {
        $token = isset($data['token']) ? $data['token'] : null;
        return new \MyTweet\Model\User(
            $data['username'],
            $data['firstname'],
            $data['lastname'],
            $data['second_lastname'],
            $data['email'],
            $data['registered_at'],
            $data['verified'],
            $data['avatar'],
            $token
        );
    }
}

?>