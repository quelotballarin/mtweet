<?php 

namespace MyTweet\Model;

class Token implements \JsonSerializable {
    
    private ?int $id;
    private string $username;
    private string $token;

    public function __construct(?int $id, string $username, string $token) {
        $this->id = $id;
        $this->username = $username;
        $this->token = $token;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function getUsername(): string {
        return $this->username;
    }

    public function setUsername(string $username) {
        $this->username = $username;
    }

    public function getToken(): string {
        return $this->token;
    }

    public function setToken(string $token) {
        $this->token = $token;
    }

    public static function fromAssoc(array $data): \MyTweet\Model\Token {
        return new \MyTweet\Model\Token(
            $data['id_token'],
            $data['username'],
            $data['token']
        );
    }

    public function jsonSerialize() {
        return [
            "idToken" => $this->id,
            "username" => $this->username,
            "token" => $this->token
        ];
    }

}
