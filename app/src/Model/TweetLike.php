<?php 

namespace MyTweet\Model;

class TweetLike implements \JsonSerializable {

    private int $tweetId;
    private string $username;
    private ?string $likedAt;

    public function __construct(int $tweetId, string $username, ?string $likedAt) {
        $this->tweetId = $tweetId;
        $this->username = $username;
        $this->likedAt = $likedAt;
    }

    public function getTweetId() {
        return $this->tweetId;
    }

    public function setTweetId(int $tweetId) {
        $this->tweetId = $tweetId;
    }

    public function getUsername(): string {
        return $this->username;
    }

    public function setUsername(string $username) {
        $this->username = $username;
    }

    public function getLikedAt(): string {
        return $this->likedAt;
    }

    public function setLikedAt(?string $likedAt) {
        $this->likedAt = $likedAt;
    }

    public function jsonSerialize() {
        return [
            "tweetId" => $this->tweetId,
            "username" => $this->username,
            "likedAt" => $this->likedAt
        ];
    }

    public static function fromAssoc(array $data): \MyTweet\Model\TweetLike {
        return new \MyTweet\Model\TweetLike(
            $data['tweet_id'],
            $data['username'],
            $data['liked_at']
        );
    }
}

?>