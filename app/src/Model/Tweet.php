<?php 

namespace MyTweet\Model;

class Tweet implements \JsonSerializable {

    private ?int $id;
    private string $text;
    private int $likes;
    private ?string $tweetedAt;
    private string $username;

    public function __construct(?int $id = null, string $text, int $likes = 0, ?string $tweetedAt = null, string $username) {
        $this->id = $id;
        $this->text = $text;
        $this->likes = $likes;
        $this->tweetedAt = $tweetedAt;
        $this->username = $username;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(?int $id): void {
        $this->id = $id;
    } 

    public function getText(): string {
        return $this->text;
    }

    public function setText(string $text): void {
        $this->text = $text;
    }

    public function getLikes(): int {
        return $this->likes;
    }

    public function setLikes(int $likes): void {
        if($likes < 0) {
            $this->likes = 0;
        } else {
            $this->likes = $likes;
        }
    }

    public function getTweetedAt(): string  {
        return $this->tweetedAt;
    }

    public function setTweetedAt(string $tweetedAt) {
        $this->tweetedAt = $tweetedAt;
    }

    public function getUsername(): string {
        return $this->username;
    }

    public function setUsername(string $username) {
        $this->username = $username;
    }

    public function jsonSerialize() {
        return [
            "id" => $this->id,
            "text" => $this->text,
            "tweetedAt" => $this->tweetedAt,
            "username" => $this->username
        ];
    }

    public static function fromAssoc(array $data): \MyTweet\Model\Tweet {
        return new \MyTweet\Model\Tweet(
            $data['tweet_id'],
            $data['tweet_text'],
            $data['tweet_likes'],
            $data['tweeted_at'],
            $data['post_user_username']
        );
    }
}